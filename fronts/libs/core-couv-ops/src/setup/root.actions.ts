import { helloSapeurThunk } from "../useCases/helloSapeur/helloSapeur.thunk";

export const actions = {
  helloSapeurThunk,
};
