# core-couv-ops

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test core-couv-ops` to execute the unit tests via [Jest](https://jestjs.io).
